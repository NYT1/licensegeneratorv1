﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace LicenseGenerator
{
    public partial class frmPath : Form
    {
        public frmPath()
        {
            InitializeComponent();
            Loadpath();
        }

        private void Loadpath()
        {
            string path = Environment.CurrentDirectory + @"//App_Data//LicensePath.txt";

            if (File.Exists(path))
            {
                StreamReader rd = new StreamReader(path);
                string FName = rd.ReadLine();
                txtPath.Text = FName;
                rd.Close();
            }
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browsedialog = new FolderBrowserDialog();
            if (browsedialog.ShowDialog() == DialogResult.OK)
            {
               txtPath.Text= (browsedialog.SelectedPath);
            }    
            
           
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

             

        private void BtnCancel_Click_1(object sender, EventArgs e)
        {

        }

        private void btnBrowse_Click_1(object sender, EventArgs e)
        {

        }

        private void BtnAddModule_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtPath.Text.Trim() == "")
                {
                    MessageBox.Show(this, "Please Select the path!", "Error", MessageBoxButtons.OK);
                }
                else
                {
                    string path = Environment.CurrentDirectory + @"//App_Data//LicensePath.txt";

                    //if (!File.Exists(path))
                    //{
                    //    File.Create(path);
                    //    TextWriter tw = new StreamWriter(path);
                    //    tw.WriteLine(txtPath.Text);
                    //    tw.Close();
                    //    MessageBox.Show("File Path Successfully Save..");
                    //}
                    if (File.Exists(path))
                    {
                        using (StreamWriter sw = new StreamWriter(path))
                            sw.WriteLine(txtPath.Text);
                        //StreamWriter tw = new StreamWriter(path);
                        //tw.WriteLine(txtPath.Text);
                        //tw.Close();

                        MessageBox.Show("File Path Successfully Save..");
                    }
                    else
                    {
                        MessageBox.Show(this, "Could not locate LicensPath.txt in Application Folder, please locate the file and try again!", "Error", MessageBoxButtons.OK);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(this, "Could not locate LicensPath.txt in Application Folder, please locate the file and try again!", "Error", MessageBoxButtons.OK);
            }
        }
    }
}
