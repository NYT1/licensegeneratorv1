﻿using LicenseGenerator.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LicenseGenerator
{
    public class Constants
    {
        public static string SoftwareId = "0";
        public static string SoftwareName = string.Empty;
      // For test and debug
     //   public static string SoftwareFileXMLPath = string.Concat(Environment.CurrentDirectory, "//App_Data//Softwares");
     //   public static string LicensesFileXMLPath = string.Concat(Environment.CurrentDirectory, "//App_Data//Licenses");

     /// <summary>
     ///  Don't assign path info until reach main form
     /// </summary>
        public static string SoftwareFileXMLPath = string.Empty;
        public static string LicensesFileXMLPath = string.Empty;



        //public static string SoftwareFileXMLPath ="C:\\Program Files (x86)\\New Year Tech\\Trak Pro 2.5 License Generator//App_Data//Softwares";
        //public static string LicensesFileXMLPath = "C:\\Program Files (x86)\\New Year Tech\\Trak Pro 2.5 License Generator//App_Data//Licenses";
        public static string ModuleName = string.Empty;
        public static List<Module> ModuleList = new List<Module>();

        //  public static XDocument SoftwareDoc;
        //  public static XDocument LicenseDoc;

        
        public static string GetSafeFileName(string FileName, char Replace = '_')
        {
            char[] invalids = Path.GetInvalidFileNameChars();
            return new string(FileName.Select(c => invalids.Contains(c) ? Replace : c).ToArray());
        }

    }
}



