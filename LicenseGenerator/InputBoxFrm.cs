﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LicenseGenerator
{
    public partial class InputBoxFrm : Form
    {
        public InputBoxFrm()
        {
            InitializeComponent();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAddModule_Click(object sender, EventArgs e)
        {
            if (this.ValidateModuleName())
            {
                Constants.ModuleName = this.TextBoxModuleName.Text.Trim();
                this.Close();
            }
        }

        private bool ValidateModuleName()
        {
            bool bStatus = true;
            if (this.TextBoxModuleName.Text.Trim() == "")
            {
                errorProviderInputModule.SetError(this.TextBoxModuleName, "Please enter Module Name");
                bStatus = false;
            }
            else
                errorProviderInputModule.SetError(this.TextBoxModuleName, "");
            return bStatus;
        }

        private void TextBoxModuleName_Validating(object sender, CancelEventArgs e)
        {
            this.ValidateModuleName();
        }
    }
}
