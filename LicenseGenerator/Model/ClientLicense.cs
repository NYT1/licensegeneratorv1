﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseGenerator.Model
{
    public class ClientLicense
    {

        private string softwareid;

        public string Softwareid
        {
            get { return softwareid; }
            set { softwareid = value; }
        }
        private string softwarename;

        public string Softwarename
        {
            get { return softwarename; }
            set { softwarename = value; }
        }
        private string clientname;

        public string Clientname
        {
            get { return clientname; }
            set { clientname = value; }
        }
        private string expirationdate;

        public string Expirationdate
        {
            get { return expirationdate; }
            set { expirationdate = value; }
        }
        private string firstname;

        public string Firstname
        {
            get { return firstname; }
            set { firstname = value; }
        }
        private string lastname;

        public string Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private int numberofclients;

        public int Numberofclients
        {
            get { return numberofclients; }
            set { numberofclients = value; }
        }
        private int numberofusers;

        public int Numberofusers
        {
            get { return numberofusers; }
            set { numberofusers = value; }
        }
        private bool istrial;

        public bool Istrial
        {
            get { return istrial; }
            set { istrial = value; }
        }

    }
}
