﻿namespace LicenseGenerator
{
    partial class InputBoxFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TextBoxModuleName = new System.Windows.Forms.TextBox();
            this.LblModuleName = new System.Windows.Forms.Label();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnAddModule = new System.Windows.Forms.Button();
            this.errorProviderInputModule = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderInputModule)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxModuleName
            // 
            this.TextBoxModuleName.Location = new System.Drawing.Point(119, 36);
            this.TextBoxModuleName.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxModuleName.Name = "TextBoxModuleName";
            this.TextBoxModuleName.Size = new System.Drawing.Size(368, 23);
            this.TextBoxModuleName.TabIndex = 0;
            this.TextBoxModuleName.Validating += new System.ComponentModel.CancelEventHandler(this.TextBoxModuleName_Validating);
            // 
            // LblModuleName
            // 
            this.LblModuleName.AutoSize = true;
            this.LblModuleName.Location = new System.Drawing.Point(13, 38);
            this.LblModuleName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblModuleName.Name = "LblModuleName";
            this.LblModuleName.Size = new System.Drawing.Size(95, 17);
            this.LblModuleName.TabIndex = 8;
            this.LblModuleName.Text = "Module Name";
            // 
            // BtnCancel
            // 
            this.BtnCancel.BackColor = System.Drawing.Color.OrangeRed;
            this.BtnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnCancel.Location = new System.Drawing.Point(291, 95);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(92, 30);
            this.BtnCancel.TabIndex = 2;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = false;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnAddModule
            // 
            this.BtnAddModule.BackColor = System.Drawing.Color.PaleGreen;
            this.BtnAddModule.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnAddModule.Location = new System.Drawing.Point(395, 95);
            this.BtnAddModule.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAddModule.Name = "BtnAddModule";
            this.BtnAddModule.Size = new System.Drawing.Size(92, 30);
            this.BtnAddModule.TabIndex = 1;
            this.BtnAddModule.Text = "Add";
            this.BtnAddModule.UseVisualStyleBackColor = false;
            this.BtnAddModule.Click += new System.EventHandler(this.BtnAddModule_Click);
            // 
            // errorProviderInputModule
            // 
            this.errorProviderInputModule.ContainerControl = this;
            // 
            // InputBoxFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 151);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnAddModule);
            this.Controls.Add(this.TextBoxModuleName);
            this.Controls.Add(this.LblModuleName);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InputBoxFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Input Module";
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderInputModule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxModuleName;
        private System.Windows.Forms.Label LblModuleName;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnAddModule;
        private System.Windows.Forms.ErrorProvider errorProviderInputModule;
    }
}