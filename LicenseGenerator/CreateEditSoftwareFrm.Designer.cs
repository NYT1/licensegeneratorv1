﻿namespace LicenseGenerator
{
    partial class CreateEditSoftwareFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TextBoxSoftwareName = new System.Windows.Forms.TextBox();
            this.LblNumberOfClients = new System.Windows.Forms.Label();
            this.LblSoftwareName = new System.Windows.Forms.Label();
            this.BtnAddNewSoftware = new System.Windows.Forms.Button();
            this.GrdModuleList = new System.Windows.Forms.DataGridView();
            this.ModuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnAddModule = new System.Windows.Forms.Button();
            this.errorPrvSoftwareName = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GrdModuleList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorPrvSoftwareName)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxSoftwareName
            // 
            this.TextBoxSoftwareName.Location = new System.Drawing.Point(147, 19);
            this.TextBoxSoftwareName.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxSoftwareName.Name = "TextBoxSoftwareName";
            this.TextBoxSoftwareName.Size = new System.Drawing.Size(327, 23);
            this.TextBoxSoftwareName.TabIndex = 0;
            this.TextBoxSoftwareName.Validating += new System.ComponentModel.CancelEventHandler(this.TextBoxSoftwareName_Validating);
            // 
            // LblNumberOfClients
            // 
            this.LblNumberOfClients.AutoSize = true;
            this.LblNumberOfClients.Location = new System.Drawing.Point(7, 113);
            this.LblNumberOfClients.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblNumberOfClients.Name = "LblNumberOfClients";
            this.LblNumberOfClients.Size = new System.Drawing.Size(61, 17);
            this.LblNumberOfClients.TabIndex = 11;
            this.LblNumberOfClients.Text = "Modules";
            // 
            // LblSoftwareName
            // 
            this.LblSoftwareName.AutoSize = true;
            this.LblSoftwareName.Location = new System.Drawing.Point(7, 19);
            this.LblSoftwareName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblSoftwareName.Name = "LblSoftwareName";
            this.LblSoftwareName.Size = new System.Drawing.Size(104, 17);
            this.LblSoftwareName.TabIndex = 10;
            this.LblSoftwareName.Text = "Software Name";
            // 
            // BtnAddNewSoftware
            // 
            this.BtnAddNewSoftware.BackColor = System.Drawing.Color.PaleGreen;
            this.BtnAddNewSoftware.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnAddNewSoftware.Location = new System.Drawing.Point(470, 365);
            this.BtnAddNewSoftware.Margin = new System.Windows.Forms.Padding(5);
            this.BtnAddNewSoftware.Name = "BtnAddNewSoftware";
            this.BtnAddNewSoftware.Size = new System.Drawing.Size(92, 30);
            this.BtnAddNewSoftware.TabIndex = 2;
            this.BtnAddNewSoftware.Text = "Add";
            this.BtnAddNewSoftware.UseVisualStyleBackColor = false;
            this.BtnAddNewSoftware.Click += new System.EventHandler(this.BtnAddNewSoftware_Click);
            // 
            // GrdModuleList
            // 
            this.GrdModuleList.AllowUserToAddRows = false;
            this.GrdModuleList.AllowUserToDeleteRows = false;
            this.GrdModuleList.AllowUserToResizeColumns = false;
            this.GrdModuleList.AllowUserToResizeRows = false;
            this.GrdModuleList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GrdModuleList.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GrdModuleList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GrdModuleList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GrdModuleList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ModuleName,
            this.Delete});
            this.GrdModuleList.Location = new System.Drawing.Point(147, 124);
            this.GrdModuleList.MultiSelect = false;
            this.GrdModuleList.Name = "GrdModuleList";
            this.GrdModuleList.ReadOnly = true;
            this.GrdModuleList.RowHeadersVisible = false;
            this.GrdModuleList.Size = new System.Drawing.Size(415, 233);
            this.GrdModuleList.TabIndex = 21;
            this.GrdModuleList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdModuleList_CellContentClick);
            // 
            // ModuleName
            // 
            this.ModuleName.DataPropertyName = "ModuleName";
            this.ModuleName.HeaderText = "Module Name";
            this.ModuleName.Name = "ModuleName";
            this.ModuleName.ReadOnly = true;
            // 
            // Delete
            // 
            this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.Delete.DataPropertyName = "Delete";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.Delete.DefaultCellStyle = dataGridViewCellStyle2;
            this.Delete.HeaderText = "";
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Delete.ToolTipText = "Delete the License";
            this.Delete.Width = 5;
            // 
            // BtnCancel
            // 
            this.BtnCancel.BackColor = System.Drawing.Color.OrangeRed;
            this.BtnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnCancel.Location = new System.Drawing.Point(369, 365);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(92, 30);
            this.BtnCancel.TabIndex = 3;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = false;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnAddModule
            // 
            this.BtnAddModule.BackColor = System.Drawing.Color.PaleGreen;
            this.BtnAddModule.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnAddModule.Location = new System.Drawing.Point(470, 86);
            this.BtnAddModule.Margin = new System.Windows.Forms.Padding(5);
            this.BtnAddModule.Name = "BtnAddModule";
            this.BtnAddModule.Size = new System.Drawing.Size(92, 30);
            this.BtnAddModule.TabIndex = 1;
            this.BtnAddModule.Text = "Add Module";
            this.BtnAddModule.UseVisualStyleBackColor = false;
            this.BtnAddModule.Click += new System.EventHandler(this.BtnAddModule_Click);
            // 
            // errorPrvSoftwareName
            // 
            this.errorPrvSoftwareName.ContainerControl = this;
            // 
            // CreateEditSoftwareFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 409);
            this.Controls.Add(this.BtnAddModule);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.GrdModuleList);
            this.Controls.Add(this.BtnAddNewSoftware);
            this.Controls.Add(this.TextBoxSoftwareName);
            this.Controls.Add(this.LblNumberOfClients);
            this.Controls.Add(this.LblSoftwareName);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CreateEditSoftwareFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Edit Software";
            this.Load += new System.EventHandler(this.CreateEditSoftware_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GrdModuleList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorPrvSoftwareName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxSoftwareName;
        private System.Windows.Forms.Label LblNumberOfClients;
        private System.Windows.Forms.Label LblSoftwareName;
        private System.Windows.Forms.Button BtnAddNewSoftware;
        private System.Windows.Forms.DataGridView GrdModuleList;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnAddModule;
        private System.Windows.Forms.ErrorProvider errorPrvSoftwareName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModuleName;
        private System.Windows.Forms.DataGridViewLinkColumn Delete;
    }
}