using System;
using System.IO;
using System.Reflection;

namespace LicenseOperations.Utilities
{
    public static class IoHelper
    {
        public static readonly string BasePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        /// <summary>
        /// Method to return the return the stream
        /// </summary>
        /// <param name="stringData"> Data as string</param>
        /// <returns></returns>
        public static Stream GetStream(string stringData)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(stringData);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Method to retrun stream as string
        /// </summary>
        /// <param name="inputStream"> Input stream</param>
        /// <returns></returns>
        public static string GetString(Stream inputStream)
        {
            string output;
            using (StreamReader reader = new StreamReader(inputStream))
            {
                output = reader.ReadToEnd();
            }
            return output;
        }


        /// <summary>
        /// Method to wrtite data to the file
        /// </summary>
        /// <param name="inputStream">Input stream</param>
        /// <param name="dataBytes"> Data in Bytes</param>
        public static void WriteStream(Stream inputStream, ref byte[] dataBytes)
        {
            using (Stream outputStream = inputStream)
            {
                outputStream.Write(dataBytes, 0, dataBytes.Length);
            }
        }

        /// <summary>
        /// Method to copy file 
        /// </summary>
        /// <param name="sourceFilePath">Source file Path</param>
        /// <param name="destFilePath">Destination File Path</param>
        public static bool CopyFile(string sourceFilePath, string destFilePath)
        {
            try
            {
                System.IO.File.Copy(sourceFilePath, destFilePath, true);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

    }
}
